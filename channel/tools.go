package channel

// Slice2Chan return a channel than contains all elements of s slice
func Slice2Chan[T any](s []T) chan T {
	ch := make(chan T, len(s))
	go func() {
		for _, t := range s {
			ch <- t
		}
		close(ch)
	}()
	return ch
}

// Filter returns a channel that contains all elements of c which obtain true with the function f
func Filter[T any](c chan T, f func(T) bool) chan T {
	res := make(chan T, cap(c))
	go func() {
		for x := range c {
			if f(x) {
				res <- x
			}
		}
		close(res)
	}()
	return res
}

// Reduce returns a value by aggregating all elements of c with the function f
func Reduce[T any](c chan T, f func(*T, *T) T) T {
	var res T
	x := <-c
	y, alive := <-c
	if !alive {
		return x
	}
	res = f(&x, &y)
	for v := range c {
		res = f(&res, &v)
	}
	return res
}

// Map returns a channel thant contains all elements of c after passing in the function f
func Map[A, B any](c chan A, f func(A) B) chan B {
	res := make(chan B, len(c))
	go func() {
		for x := range c {
			res <- f(x)
		}
		close(res)
	}()
	return res
}

